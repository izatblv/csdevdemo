<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
            integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
</head>
<body>
<?php
require "ajax.php" ?>
<div class="container">
    <h1>Добавить запись</h1>
    <div class="container">
        <table>
            <tr>
                <td>Имя:</td>
                <td><input id="name" type="text"></td>
            </tr>
            <tr>
                <td>Фамилия:</td>
                <td><input id="lastname"></td>
            </tr>
            <tr>
                <td>Баланс:</td>
                <td><input id="balance"></td>
            </tr>
        </table>
        <br>
        <div class="buttons_update row">
            <div><strong>Добавить запись? </strong></div>
            <button onclick="q_new()">ОК</button>
            <a href="/">
                <button>Отмена</button>
            </a>
        </div>
        <br>
        <div class="buttons row">
            <a href="/">
                <button>Назад</button>
            </a>
        </div>
    </div>


</div>

</body>
<script>
    function q_new() {
        $.ajax({
            url: "ajax.php",
            type: "POST",
            data: ({
                f: "q_new_record",
                lastname: $("#lastname").val(),
                balance: $("#balance").val(),
                firstname: $("#name").val()
            }),
            dataType: "html"
            // beforeSend: f_before,
            // success: f_success
        });
        location.href = '/';
    }

    // $("input").wrap("<div class='row col-md-12'></div>");
    $("input").addClass("form-control");
    $("button").addClass("btn btn-outline-dark");
</script>
</html>