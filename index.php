<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta firstname="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
            integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
</head>
<body>
<div class="container">
    <h1>CSDEV</h1>
    <div>
        <div class="inputs row">
            <input id="id" type="text" placeholder="id">
            <input id="firstname" type="text" placeholder="Имя">
            <input id="lastname" type="text" placeholder="Фамилия">
            <input id="balance" type="text" placeholder="Баланс">
            <select id="balance_select">
                <option value="b=">=</option>
                <option value="b>=">>=</option>
                <option value="b<="><=</option>
            </select>
        </div>
        <br>
        <div class="buttons row">
            <button onclick="q_clear()">Очистить</button>
            <button id="q_all">Вся база</button>
            <a href="/new_record.php">
                <button>Добавить запись</button>
            </a>
        </div>
        <div class="sort">
            Сортировать по
            <select id="sort">
                <option value="id">id</option>
                <option value="firstname">Имени</option>
                <option value="lastname">Фамилии</option>
                <option value="balance">Балансу</option>
            </select>
            <select id="sort_order">
                <option value="ASC">abc</option>
                <option value="DESC">cba</option>
            </select>
        </div>
    </div>

    <div id="information">
    </div>
    <div>
</body>
<script>
    function q_clear() {
        $("#id").val("");
        $("#name").val("");
        $("#lastname").val("");
        $("#balance").val("");
        location.reload();
    }

    function f_before() {
        $("#information").text("Загрука...")
    }

    function f_success(data) {
        $("#information").html(data)
    }

    $(document).ready(function () {
        $.ajax({
            url: "ajax.php",
            type: "POST",
            data: ({f: "q_last"}),
            dataType: "html",
            beforeSend: f_before,
            success: f_success
        });
        $("#q_all").bind("click", function () {
                $.ajax({
                    url: "ajax.php",
                    type: "POST",
                    data: ({
                        f: "q_all",
                        sort_by: $("#sort").val(),
                        sort_order: $("#sort_order").val()
                    }),
                    dataType: "html",
                    beforeSend: f_before,
                    success: f_success
                });
            }
        )
    });
    $("input").change(function () {
        $.ajax({
            url: "ajax.php",
            type: "POST",
            data: ({
                f: "q_find",
                id: $("#id").val(),
                lastname: $("#lastname").val(),
                balance: $("#balance").val(),
                balance_select: $("#balance_select").val(),
                sort_by: $("#sort").val(),
                sort_order: $("#sort_order").val(),
                firstname: $("#firstname").val()
            }),
            dataType: "html",
            beforeSend: f_before,
            success: f_success
        });
    });
    $("input").wrap("<div class='row col-md-2'></div>");
    $("input").addClass("form-control");
    $("button").addClass("btn btn-outline-dark");

    $(".sort select").change(function () {
        localStorage.sort = $("#sort").val();
        localStorage.sort_order = $("#sort_order").val();
    });
    if(localStorage.sort_order!="DESC"||localStorage.sort_order!="ASC"){
        localStorage.sort="id";
        localStorage.sort_order="DESC";
    }
    $("#sort").val(localStorage.sort);
    $("#sort_order").val(localStorage.sort_order);
</script>
</html>
