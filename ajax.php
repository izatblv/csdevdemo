<?php
require "db_config.php";

//распечатка элемента базы данных
function p_db_key($el, $key)
{
    echo "Find by key $key<br>";
    while (($row = $el->fetch_assoc()) != false) {
        echo "id=" . $row["id"] . " " . $row["$key"];
        echo "<br>";
    }
    echo "Total db rows=" . $el->num_rows . "<br>";
}

//распечатка строки базы данных
function p_db($el)
{
    while (($row = $el->fetch_assoc()) != false) {
        print_r($row);
        echo "<br>";
    }
    echo "Total db rows=" . $el->num_rows . "<br>";
}

$mysqli = new mysqli($servername, $username, $password, $dbname);
$mysqli->query("SET NAMES 'utf8'");

function show_table($db)
{
    ?>
    <table class="table table-hover">
        <tbody>
        <tr class="active">
            <th>#</th>
            <th>id</th>
            <th>Имя</th>
            <th>Фамилия</th>
            <th>Баланс</th>
            <th></th>
        </tr>
        <tr></tr>
        <?php $i = 1 ?>
        <?php while (($row = $db->fetch_assoc()) != false) { ?>
            <tr>
                <td><?= $i ?></td>
                <td><?= $row['id'] ?></td>
                <td><?= $row['firstname'] ?></td>
                <td><?= $row['lastname'] ?></td>
                <td><?= $row['balance'] ?></td>
                <td><a href="/show.php?id=<?= $row['id'] ?>">Просмотр</a></td>
            </tr>
            <?php $i++ ?>
        <?php } ?>
        </tbody>
    </table>

<?php } ?>

<?php
//вся база
function q_all()
{
    global $mysqli;
    $db = $mysqli->query("SELECT * FROM `users`
    ORDER BY ".$_POST['sort_by']." ".$_POST['sort_order']."
");
    show_table($db);
}

function q_last()
{
    global $mysqli;
    $db = $mysqli->query("SELECT * FROM `users` ORDER BY id DESC LIMIT 5");
    show_table($db);
}

?>
<?php
function q_find_id($id)
{
    global $mysqli;
    $db = $mysqli->query("SELECT * FROM `users` WHERE `id`='$id'");
    $row = $db->fetch_assoc();
    ?>
    <div class="container">
        <h1>Данные о клиенте</h1>
        <div class="container">
            <table>
                <tr>
                    <td>id:</td>
                    <td><input id="id" class='disabled' type="text" value="<?= $row['id'] ?>" disabled></td>
                </tr>
                <tr>
                    <td>Имя:</td>
                    <td><input id="firstname" type="text" value="<?= $row['firstname'] ?>" disabled></td>
                </tr>
                <tr>
                    <td>Фамилия:</td>
                    <td><input id="lastname" type="text" value="<?= $row['lastname'] ?>" disabled></td>
                </tr>
                <tr>
                    <td>Баланс:</td>
                    <td><input id="balance" type="text" value="<?= $row['balance'] ?>" disabled></td>
                </tr>
            </table>
            <br>
            <div class="buttons_update row">
                <div><strong>Править? </strong></div>
                <button id="q_update">ОК</button>
                <button id="q_update_cancel">Отмена</button>
            </div>
            <div class="buttons_delete row">
                <div><strong>Удалить? </strong></div>
                <button id="q_delete">ОК</button>
                <button id="q_delete_cancel">Отмена</button>
            </div>
            <div class="buttons row">
                <a href="/">
                    <button>Назад</button>
                </a>
                <button onclick="input_edit()">Править</button>
                <button onclick="input_delete()">Удалить</button>
                <a onclick="window.print()">
                    <button>Печать</button>
                </a>
            </div>
        </div>


    </div>
    <?php
}

function q_update()
{
    global $mysqli;
    $db = $mysqli->query("UPDATE `users` SET firstname='" . $_POST['firstname'] . "',
    lastname='" . $_POST['lastname'] . "',
    balance='" . $_POST['balance'] . "'
    WHERE id=" . $_POST['id']);
}

function q_new_record()
{
    global $mysqli;
    $db = $mysqli->query("INSERT INTO `users` (`id`, `firstname`, `lastname`, `balance`) VALUES (NULL, '" . $_POST['firstname'] . "', '" . $_POST['lastname'] . "', '" . $_POST['balance'] . "')");
}
function q_delete()
{
    global $mysqli;
    $db = $mysqli->query("DELETE FROM `users` WHERE `users`.`id` = " . $_POST['id']);
}
function q_find()
{
    $i = 0;
    $where = "";
    foreach ($_POST as $key => $value) {
        if ($key == "f") continue;
        if ($key == "balance_select") continue;
        if ($key == "sort_by") continue;
        if ($key == "sort_order") continue;
        if ($value == "") continue;
        if ($i != 0) {
            $where .= " AND ";
        }
        if ($key == "id") {
            $where .= "(`id`='" . $_POST['id'] . "')";
        }
        if ($key == "firstname" || $key == "lastname") {
            $where .= "(`$key` LIKE '" . $value . "%')";
        }
        if ($key == "balance" && $_POST['balance_select'] == 'b=') {
            $where .= "(`" . $key . "`='" . $value . "')";
        }
        if ($key == "balance" && $_POST['balance_select'] == 'b>=') {
            $where .= "(`" . $key . "`>='" . $value . "')";
        }
        if ($key == "balance" && $_POST['balance_select'] == 'b<=') {
            $where .= "(`" . $key . "`<='" . $value . "')";
        }
        $i++;
    }
    if ($where != "") {
        global $mysqli;
        $db = $mysqli->query("
        SELECT *
        FROM `users`
        WHERE ".$where."
        ORDER BY ".$_POST['sort_by']." ".$_POST['sort_order']."
 ");
        show_table($db);
    }
}

?>
<?php
switch ($_POST['f']) {
    case "q_all":
        q_all();
        break;
    case "q_find":
        q_find();
        break;
    case "q_update":
        q_update();
        break;
    case "q_last":
        q_last();
        break;
    case "q_delete":
        q_delete();
        break;
    case "q_new_record":
        q_new_record();
        break;
}
?>